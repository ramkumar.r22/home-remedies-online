﻿using System;
using System.Text.RegularExpressions;

namespace HomeRemediesOnline.Common
{
    public class Common
    {
        public bool IsValidEmailID(string emailaddress)
        {
            try
            {
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(emailaddress);
                if (match.Success)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}
