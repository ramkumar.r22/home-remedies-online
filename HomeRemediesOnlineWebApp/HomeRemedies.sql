USE [master]
GO
/****** Object:  Database [HomeRemedies]    Script Date: 9/7/2020 6:40:21 PM ******/
CREATE DATABASE [HomeRemedies]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'HomeRemedies', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER01\MSSQL\DATA\HomeRemedies.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'HomeRemedies_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER01\MSSQL\DATA\HomeRemedies_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [HomeRemedies] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [HomeRemedies].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [HomeRemedies] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [HomeRemedies] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [HomeRemedies] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [HomeRemedies] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [HomeRemedies] SET ARITHABORT OFF 
GO
ALTER DATABASE [HomeRemedies] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [HomeRemedies] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [HomeRemedies] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [HomeRemedies] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [HomeRemedies] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [HomeRemedies] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [HomeRemedies] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [HomeRemedies] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [HomeRemedies] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [HomeRemedies] SET  DISABLE_BROKER 
GO
ALTER DATABASE [HomeRemedies] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [HomeRemedies] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [HomeRemedies] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [HomeRemedies] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [HomeRemedies] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [HomeRemedies] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [HomeRemedies] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [HomeRemedies] SET RECOVERY FULL 
GO
ALTER DATABASE [HomeRemedies] SET  MULTI_USER 
GO
ALTER DATABASE [HomeRemedies] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [HomeRemedies] SET DB_CHAINING OFF 
GO
ALTER DATABASE [HomeRemedies] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [HomeRemedies] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [HomeRemedies] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'HomeRemedies', N'ON'
GO
ALTER DATABASE [HomeRemedies] SET QUERY_STORE = OFF
GO
USE [HomeRemedies]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 9/7/2020 6:40:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[CID] [int] IDENTITY(1,1) NOT NULL,
	[FName] [varchar](50) NOT NULL,
	[LName] [varchar](50) NOT NULL,
	[Phone] [varchar](50) NOT NULL,
	[Address1] [varchar](50) NOT NULL,
	[Address2] [varchar](50) NULL,
	[Suburb] [varchar](50) NOT NULL,
	[Postcode] [varchar](50) NOT NULL,
	[State] [varchar](50) NOT NULL,
	[Ctype] [varchar](50) NOT NULL,
	[CardNo] [varchar](50) NOT NULL,
	[ExpDate] [datetime] NOT NULL,
	[Email] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[CID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order_Products]    Script Date: 9/7/2020 6:40:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order_Products](
	[OrderID] [int] NOT NULL,
	[PID] [int] NOT NULL,
	[Qty] [int] NOT NULL,
	[TotalSale] [money] NOT NULL,
 CONSTRAINT [PK_Order_Products] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC,
	[PID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 9/7/2020 6:40:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[OrderDate] [datetime] NOT NULL,
	[DeliveryDate] [datetime] NOT NULL,
	[CID] [int] NOT NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 9/7/2020 6:40:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[PID] [int] IDENTITY(1,1) NOT NULL,
	[PName] [varchar](50) NOT NULL,
	[Brand] [varchar](50) NOT NULL,
	[UnitPrice] [money] NOT NULL,
	[UnitsInStock] [int] NOT NULL,
	[Category] [varchar](50) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[SID] [int] NOT NULL,
	[ROL] [int] NOT NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[PID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShoppingCartData]    Script Date: 9/7/2020 6:40:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShoppingCartData](
	[TempOrderID] [int] IDENTITY(1,1) NOT NULL,
	[PID] [int] NOT NULL,
	[PName] [varchar](50) NOT NULL,
	[UnitPrice] [money] NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_ShoppingCartData] PRIMARY KEY CLUSTERED 
(
	[TempOrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Suppliers]    Script Date: 9/7/2020 6:40:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Suppliers](
	[SID] [int] IDENTITY(1,1) NOT NULL,
	[SName] [varchar](50) NOT NULL,
	[Phone] [varchar](50) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Address1] [varchar](50) NOT NULL,
	[Address2] [varchar](50) NULL,
	[Suburb] [varchar](50) NOT NULL,
	[State] [varchar](50) NOT NULL,
	[Postcode] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Suppliers] PRIMARY KEY CLUSTERED 
(
	[SID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([CID], [FName], [LName], [Phone], [Address1], [Address2], [Suburb], [Postcode], [State], [Ctype], [CardNo], [ExpDate], [Email]) VALUES (1, N'Ramkumar', N'R', N'9874653210', N'Chennai', N'Sembakkam', N'Tambaram', N'600073', N'Tamil Nadu', N'VISA', N'1234567890123456', CAST(N'2015-12-12T00:00:00.000' AS DateTime), N'ram@gmail.com')
INSERT [dbo].[Customers] ([CID], [FName], [LName], [Phone], [Address1], [Address2], [Suburb], [Postcode], [State], [Ctype], [CardNo], [ExpDate], [Email]) VALUES (2, N'Prasannakumar', N'R', N'5463217890', N'Chennai', N'Perumbakkam', N'Medavakkam', N'600085', N'Tamil Nadu', N'VISA', N'1234567890123456', CAST(N'2018-05-14T00:00:00.000' AS DateTime), N'prasanna@yahoo.com')
INSERT [dbo].[Customers] ([CID], [FName], [LName], [Phone], [Address1], [Address2], [Suburb], [Postcode], [State], [Ctype], [CardNo], [ExpDate], [Email]) VALUES (47, N'Jennifer', N'Jones', N'0425987412', N'12', N'School Street', N'Richmond', N'3200', N'VIC', N'AMEX', N'123456789123456', CAST(N'2012-06-09T00:00:00.000' AS DateTime), N'Jen@yahoo.com')
SET IDENTITY_INSERT [dbo].[Customers] OFF
GO
INSERT [dbo].[Order_Products] ([OrderID], [PID], [Qty], [TotalSale]) VALUES (52, 12, 1, 200.0000)
GO
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([OrderID], [OrderDate], [DeliveryDate], [CID]) VALUES (52, CAST(N'2012-05-24T10:44:22.093' AS DateTime), CAST(N'2012-05-26T00:00:00.000' AS DateTime), 47)
SET IDENTITY_INSERT [dbo].[Orders] OFF
GO
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([PID], [PName], [Brand], [UnitPrice], [UnitsInStock], [Category], [Description], [SID], [ROL]) VALUES (3, N'BacoMind', N'Natural', 300.0000, 49, N'Brain', N'Sustainable and clinically researched brain health', 2, 10)
INSERT [dbo].[Products] ([PID], [PName], [Brand], [UnitPrice], [UnitsInStock], [Category], [Description], [SID], [ROL]) VALUES (10, N'Oci Best', N'The Himalaya', 55.0000, 0, N'Brain', N'Supports Stress Management Naturally', 6, 10)
INSERT [dbo].[Products] ([PID], [PName], [Brand], [UnitPrice], [UnitsInStock], [Category], [Description], [SID], [ROL]) VALUES (12, N'AP-Bio', N'The Himalaya', 200.0000, 4, N'Body', N'Immune Support', 6, 5)
INSERT [dbo].[Products] ([PID], [PName], [Brand], [UnitPrice], [UnitsInStock], [Category], [Description], [SID], [ROL]) VALUES (15, N'Kalm Cold', N'Dabur', 45.0000, 10, N'Cold', N'Natural support to manage symptoms of common cold', 12, 10)
SET IDENTITY_INSERT [dbo].[Products] OFF
GO
SET IDENTITY_INSERT [dbo].[ShoppingCartData] ON 

INSERT [dbo].[ShoppingCartData] ([TempOrderID], [PID], [PName], [UnitPrice], [Quantity]) VALUES (1, 3, N'Semi Automatic Washing Machine', 300.0000, 1)
INSERT [dbo].[ShoppingCartData] ([TempOrderID], [PID], [PName], [UnitPrice], [Quantity]) VALUES (2, 12, N'Sunbeam Microwave', 200.0000, 1)
SET IDENTITY_INSERT [dbo].[ShoppingCartData] OFF
GO
SET IDENTITY_INSERT [dbo].[Suppliers] ON 

INSERT [dbo].[Suppliers] ([SID], [SName], [Phone], [Email], [Address1], [Address2], [Suburb], [State], [Postcode]) VALUES (2, N'Natural', N'0425874123', N'Natural@gmail.com', N'Chennai', NULL, N'', N'Tamil Nadu', N'600073')
INSERT [dbo].[Suppliers] ([SID], [SName], [Phone], [Email], [Address1], [Address2], [Suburb], [State], [Postcode]) VALUES (6, N'The Himalaya', N'0425678915', N'TheHimalaya@gmail.com', N'Chennai', NULL, N'', N'Tamil Nadu', N'600074')
INSERT [dbo].[Suppliers] ([SID], [SName], [Phone], [Email], [Address1], [Address2], [Suburb], [State], [Postcode]) VALUES (12, N'Dabur', N'0442545466', N'Dabur@gmail.com', N'Chennai', NULL, N'', N'Tamil Nadu', N'600075')
SET IDENTITY_INSERT [dbo].[Suppliers] OFF
GO
ALTER TABLE [dbo].[Order_Products]  WITH CHECK ADD  CONSTRAINT [FK_Order_Products_Orders] FOREIGN KEY([OrderID])
REFERENCES [dbo].[Orders] ([OrderID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Order_Products] CHECK CONSTRAINT [FK_Order_Products_Orders]
GO
ALTER TABLE [dbo].[Order_Products]  WITH CHECK ADD  CONSTRAINT [FK_Order_Products_Products] FOREIGN KEY([PID])
REFERENCES [dbo].[Products] ([PID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Order_Products] CHECK CONSTRAINT [FK_Order_Products_Products]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Customers] FOREIGN KEY([CID])
REFERENCES [dbo].[Customers] ([CID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Customers]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Suppliers] FOREIGN KEY([SID])
REFERENCES [dbo].[Suppliers] ([SID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Suppliers]
GO
USE [master]
GO
ALTER DATABASE [HomeRemedies] SET  READ_WRITE 
GO
