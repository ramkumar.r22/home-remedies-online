USE [HomeRemedies]
GO
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([CID], [FName], [LName], [Phone], [Address1], [Address2], [Suburb], [Postcode], [State], [Ctype], [CardNo], [ExpDate], [Email]) VALUES (1, N'Ramkumar', N'R', N'9874653210', N'Chennai', N'Sembakkam', N'Tambaram', N'600073', N'Tamil Nadu', N'VISA', N'1234567890123456', CAST(N'2015-12-12T00:00:00.000' AS DateTime), N'ram@gmail.com')
INSERT [dbo].[Customers] ([CID], [FName], [LName], [Phone], [Address1], [Address2], [Suburb], [Postcode], [State], [Ctype], [CardNo], [ExpDate], [Email]) VALUES (2, N'Prasannakumar', N'R', N'5463217890', N'Chennai', N'Perumbakkam', N'Medavakkam', N'600085', N'Tamil Nadu', N'VISA', N'1234567890123456', CAST(N'2018-05-14T00:00:00.000' AS DateTime), N'prasanna@yahoo.com')
INSERT [dbo].[Customers] ([CID], [FName], [LName], [Phone], [Address1], [Address2], [Suburb], [Postcode], [State], [Ctype], [CardNo], [ExpDate], [Email]) VALUES (47, N'Jennifer', N'Jones', N'0425987412', N'12', N'School Street', N'Richmond', N'3200', N'VIC', N'AMEX', N'123456789123456', CAST(N'2012-06-09T00:00:00.000' AS DateTime), N'Jen@yahoo.com')
SET IDENTITY_INSERT [dbo].[Customers] OFF
GO
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([OrderID], [OrderDate], [DeliveryDate], [CID]) VALUES (52, CAST(N'2012-05-24T10:44:22.093' AS DateTime), CAST(N'2012-05-26T00:00:00.000' AS DateTime), 47)
SET IDENTITY_INSERT [dbo].[Orders] OFF
GO
SET IDENTITY_INSERT [dbo].[Suppliers] ON 

INSERT [dbo].[Suppliers] ([SID], [SName], [Phone], [Email], [Address1], [Address2], [Suburb], [State], [Postcode]) VALUES (2, N'Natural', N'0425874123', N'Natural@gmail.com', N'Chennai', NULL, N'', N'Tamil Nadu', N'600073')
INSERT [dbo].[Suppliers] ([SID], [SName], [Phone], [Email], [Address1], [Address2], [Suburb], [State], [Postcode]) VALUES (6, N'The Himalaya', N'0425678915', N'TheHimalaya@gmail.com', N'Chennai', NULL, N'', N'Tamil Nadu', N'600074')
INSERT [dbo].[Suppliers] ([SID], [SName], [Phone], [Email], [Address1], [Address2], [Suburb], [State], [Postcode]) VALUES (12, N'Dabur', N'0442545466', N'Dabur@gmail.com', N'Chennai', NULL, N'', N'Tamil Nadu', N'600075')
SET IDENTITY_INSERT [dbo].[Suppliers] OFF
GO
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([PID], [PName], [Brand], [UnitPrice], [UnitsInStock], [Category], [Description], [SID], [ROL]) VALUES (3, N'BacoMind', N'Natural', 300.0000, 49, N'Brain', N'Sustainable and clinically researched brain health', 2, 10)
INSERT [dbo].[Products] ([PID], [PName], [Brand], [UnitPrice], [UnitsInStock], [Category], [Description], [SID], [ROL]) VALUES (10, N'Oci Best', N'The Himalaya', 55.0000, 0, N'Brain', N'Supports Stress Management Naturally', 6, 10)
INSERT [dbo].[Products] ([PID], [PName], [Brand], [UnitPrice], [UnitsInStock], [Category], [Description], [SID], [ROL]) VALUES (12, N'AP-Bio', N'The Himalaya', 200.0000, 4, N'Body', N'Immune Support', 6, 5)
INSERT [dbo].[Products] ([PID], [PName], [Brand], [UnitPrice], [UnitsInStock], [Category], [Description], [SID], [ROL]) VALUES (15, N'Kalm Cold', N'Dabur', 45.0000, 10, N'Cold', N'Natural support to manage symptoms of common cold', 12, 10)
SET IDENTITY_INSERT [dbo].[Products] OFF
GO
INSERT [dbo].[Order_Products] ([OrderID], [PID], [Qty], [TotalSale]) VALUES (52, 12, 1, 200.0000)
GO
SET IDENTITY_INSERT [dbo].[ShoppingCartData] ON 

INSERT [dbo].[ShoppingCartData] ([TempOrderID], [PID], [PName], [UnitPrice], [Quantity]) VALUES (1, 3, N'Semi Automatic Washing Machine', 300.0000, 1)
INSERT [dbo].[ShoppingCartData] ([TempOrderID], [PID], [PName], [UnitPrice], [Quantity]) VALUES (2, 12, N'Sunbeam Microwave', 200.0000, 1)
SET IDENTITY_INSERT [dbo].[ShoppingCartData] OFF
GO
