﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeRemediesOnlineWebApp.Models
{
    public class OrderModel : HomeRemediesOnlineWebApp.Order
    {

        public decimal TotalPayment
        {
            get
            {
                return this.Order_Products.Sum(p => p.TotalSale);
            }
        }
    }
}