﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HomeRemediesOnlineWebApp.Areas.Admin.Controllers
{
    public class CustomerController : BaseController
    {
        public CustomerController()
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<Customer, Models.Customer>();
                cfg.CreateMap<Models.Customer, Customer>();
            });

            ViewBag.Customers = _ctx.Customers;
        }

        // GET: Admin/Customer
        public ActionResult Index()
        {
            var customersDB = _ctx.Customers.ToList();
            var model = Mapper.Map<List<Customer>, IEnumerable<Models.Customer>>(customersDB);
            return View(model);
        }

        private Customer _getCustomer(int id)
        {
            var customer = _ctx.Customers.FirstOrDefault(s => s.CID == id);
            return customer;
        }

        // GET: Admin/Customer/Details/5
        public ActionResult Details(int id)
        {
            var model = _getCustomer(id);
            return View(model);
        }

        // GET: Admin/Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Customer/Create
        [HttpPost]
        public ActionResult Create(Customer customer)
        {
            try
            {
                // TODO: Add insert logic here
                _ctx.Customers.Add(customer);
                _ctx.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View(customer);
            }
        }

        // GET: Admin/Customer/Edit/5
        public ActionResult Edit(int id)
        {
            var model = _getCustomer(id);
            return View(model);
        }

        // POST: Admin/Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Customer model)
        {
            try
            {
                // TODO: Add update logic here
                Customer customer = _ctx.Customers.FirstOrDefault(s => s.CID == id);
                customer.FName = model.FName;
                customer.LName = model.LName;
                customer.Phone = model.Phone;
                customer.Address1 = model.Address1;
                customer.Address2 = model.Address2;
                customer.Suburb = model.Suburb;
                customer.Postcode = model.Postcode;
                customer.State = model.State;
                customer.Ctype = model.Ctype;
                customer.ExpDate = model.ExpDate;
                customer.Email = model.Email;

                _ctx.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(model);
            }
        }

        // GET: Admin/Customer/Delete/5
        public ActionResult Delete(int id)
        {
            var customer = _getCustomer(id);
            _ctx.Customers.Remove(customer);
            _ctx.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
