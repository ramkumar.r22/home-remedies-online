﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeRemediesOnlineWebApp
{
    public partial class Product
    {
        private HomeRemediesEntities _ctx = new HomeRemediesEntities();
        public List<Product> All
        {
            get
            {
                return _ctx.Products.ToList<Product>();

            }
        }
    }
}