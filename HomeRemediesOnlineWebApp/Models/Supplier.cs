﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeRemediesOnlineWebApp
{
    public partial class Supplier
    {
        private HomeRemediesEntities _ctx = new HomeRemediesEntities();

        public IEnumerable<Supplier> All
        {
            get
            {
                return _ctx.Suppliers;
            }
        }
    }
}