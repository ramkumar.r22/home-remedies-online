﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HomeRemediesOnlineWebApp.Startup))]
namespace HomeRemediesOnlineWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
