﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HomeRemediesOnlineWebApp.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeRemediesOnline.Common;

namespace HomeRemediesOnlineWebApp.Controllers.HomeTests
{
    [TestClass()]
    public class HomeTests
    {
        [TestMethod()]
        public void IsValidEmailIDTest()
        {
            Common common = new Common();
            Assert.AreEqual(common.IsValidEmailID("ramkumar.r22@gmail.com"), true);
        }
    }
}